from django.urls import path
from . import views
from .views.weatherView import weatherByCoOrds
from .views.weatherView import weatherForCity
from .views.index import index

urlpatterns = [
    path('', index),
    path('weatherByCoOrds/', weatherByCoOrds),
    path('weatherForCity/', weatherForCity),
    path('getDefaultWeather/', views.weatherView.getDefaultWeather, name='getDefaultWeather'),
    # path('getWeatherForCity/', views.weatherView.getWeatherForCity, name='getWeatherForCity'),
]
