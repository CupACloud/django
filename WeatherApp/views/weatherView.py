from django.shortcuts import render
from django.http import HttpResponse

import urllib.request
import json
import urllib.parse

# RETRIEVE WEATHER FOR CITY (DJANGO SPECIFIC BINDING)
def weatherForCity(request):
    
    if request.method == 'POST':

        city = request.POST['city']
        if city != '':
            params = {'q': city, 'units': 'metric', 'appid' : '067332d8a8d5fc5d4cd7b719ddd644bc'}
            urlParams = urllib.parse.urlencode(params) 
            url ='https://api.openweathermap.org/data/2.5/weather?'+urlParams
            try:                
                source = urllib.request.urlopen(url).read()
                list_of_data =json.loads(source)

                data ={
                    'country_code':str(list_of_data['sys']['country']),
                    'temp':str(list_of_data["main"]['temp']),
                    'pressure':str(list_of_data['main']["pressure"]),
                    'humidity':str(list_of_data['main']['humidity']),
                    'main':str(list_of_data["weather"][0]['main']),
                    'description':str(list_of_data["weather"][0]['description']),
                    'city':list_of_data["name"]
                }
            except:
                data ={
                    'country_code':'Not Found',
                    'temp':'Not Found',
                    'pressure':'Not Found',
                    'humidity':'Not Found',
                    'main':'Not Found',
                    'description':'Not Found',
                    'city':'Not Found'
                }            
        else:
            data ={}    
    else:
        data ={}
    return render(request,'weatherForCity.html',data)



# RETRIEVE WEATHER USING LOCATION (LAT,LONG) AJAX

def weatherByCoOrds(request):    
    return render(request,'weatherCoOrds.html',{})

def getDefaultWeather(request):

    if request.method == 'POST':

        lat = request.POST.get('lat', '0.0')
        lon = request.POST.get('lon', '0.0')
        if lat == "0.0" or lon == "0.0":
            data ={}
            return HttpResponse(json.dumps(data), content_type="application/json")
        
        params = {'lat': lat,'lon': lon, 'units': 'metric', 'appid' : '067332d8a8d5fc5d4cd7b719ddd644bc'}
        urlParams = urllib.parse.urlencode(params) 
        url ='http://api.openweathermap.org/data/2.5/weather?'+urlParams
        source = urllib.request.urlopen(url).read()
        # source = urllib.request.urlopen('http://api.openweathermap.org/data/2.5/weather?lat='+lat+'&lon='+lon+'&appid=067332d8a8d5fc5d4cd7b719ddd644bc&units=metric').read()
        list_of_data =json.loads(source)

        data ={
            'country_code':str(list_of_data['sys']['country']),
            'cor':str(list_of_data["coord"]["lon"])+" "+str(list_of_data["coord"]["lat"]),
            'temp':str(list_of_data["main"]['temp']),
            'pressure':str(list_of_data['main']["pressure"]),
            'humidity':str(list_of_data['main']['humidity']),
            'main':str(list_of_data["weather"][0]['main']),
            'description':str(list_of_data["weather"][0]['description']),
            'icon':list_of_data["weather"][0]['icon'],
            'city':list_of_data["name"]
        }
    else:
        data ={}
    return HttpResponse(json.dumps(data), content_type="application/json")
