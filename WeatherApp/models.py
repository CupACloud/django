from django.db import models

class Note(models.Model):
    title = models.CharField(max_length=25, unique=True)
    text = models.CharField(max_length=4000)

    def __str__(self):
        return f"Title: {self.title}"
